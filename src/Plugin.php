<?php

return [
    'name' => 'Test',
    'version' => '1.0.0',
    'author' => 'Farhad Ziaee',
    'description' => 'This is just for Test a schematic of Toolara Plugins',
    'package' => 'toolara/test',
    'namespace' => 'Toolara\Test',
    'author_email'=>'fardzia@gmail.com',
    'type' => 'plugin',
    'url' => 'https://gitlab.com',
    'providers' => [
        '\Toolara\Test\ToolaraTestServiceProvider'
    ]
];
