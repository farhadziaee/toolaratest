<?php

namespace Toolara\Test\Http\Controllers;

use App\Http\Controllers\Controller;

class TestController extends Controller
{
    public function __construct()
    {
    }

    public function test()
    {
        return view("toolara_test::test");
    }

}
