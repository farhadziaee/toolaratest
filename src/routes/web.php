<?php

use Illuminate\Support\Facades\Route;



Route::group(['namespace' => 'Toolara\Test\Http\Controllers','middleware'=>'toolara'], function () {
    Route::prefix("/backend/test")->middleware('auth')->group(function () {
        Route::get('/', 'TestController@test')->name('test');
    });
});
